import uuid
import re

from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response, JSONResponse

# CSRF認証を担当するミドルウェア
class CSRFMiddleware(BaseHTTPMiddleware):

    # トークンの検証をする
    def validate_token(self, token_header:str|None, token_cookie:str|None) -> str|None:

        # ヘッダで送られてきたトークンが正常なUUIDでかつCOOKIEのものと等しければOK
        uuid_regex = r'^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-r[0-9a-f]{3}-[0-9a-f]{12}$'
        if not token_header: return 'No CSRF token is set.'
        if re.fullmatch(uuid_regex, token_header): return 'Invalid CSRF token.'
        if token_header != token_cookie: return 'CSRF token does not match.'

    # リクエストに対するディスパッチ処理
    async def dispatch(self, request:Request, call_next:RequestResponseEndpoint) -> Response:
        
        # トークンを取得
        token_created = False
        token_cookie = request.cookies.get('csrftoken', None)
        token_header = request.headers.get('X-CSRF-Token', None)

        # COOKIEにトークンが設定されていなければ生成
        if not token_cookie:
            token_cookie = str(uuid.uuid4())
            token_created = True

        # セーフメソッドでない場合、トークンの検証に失敗したならばエラーメッセージを返す
        if not request.method in ['GET','HEAD','OPTIONS','TRACE']:
            message = self.validate_token(token_header, token_cookie)
            if message: return JSONResponse({'message':message}, status_code=403)

        # レスポンスの生成処理を実行
        request.state.csrftoken = token_cookie
        response = await call_next(request)

        # 生成されたCSRFトークンをCOOKIEに保存
        if token_created:
            response.set_cookie('csrftoken', token_cookie, 864000, path='/', secure=True, samesite='strict')

        # レスポンスを返す
        return response