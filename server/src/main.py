import uuid

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.requests import Request
from fastapi.responses import Response, JSONResponse

from middlewares import CSRFMiddleware

# アプリケーションの起動
app = FastAPI()

# CORS対策:オリジンを追加
origins = [
    'http://127.0.0.1:5173', # デバッグ用
    'http://localhost:5173', # デバッグ用
]

# ミドルウェアの追加
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)
app.add_middleware(CSRFMiddleware)

# ルートエンドポイント
@app.get('/')
async def root_get(request:Request) -> Response:
    return JSONResponse({'message':'Hello, World!'})

# CSRFトークンを取得
@app.get('/csrf')
async def csrftoken(request:Request) -> Response:
    token = str(request.state.csrftoken)
    return JSONResponse({'token':token})

# ログイン
@app.post('/login')
async def login(request:Request) -> Response:

    body = dict(await request.body())

    return JSONResponse({
        'id': str(uuid.uuid4()),
        'discord_id': str(0x7FFFFFFFFFFFFFFF),
        'player_count': 0,
        'master_count': 0,
    })

# ユーザー情報を取得
@app.get('/user')
async def get_user(request:Request) -> Response:
    pass

# ユーザー情報を取得
@app.patch('/user')
async def update_user(request:Request) -> Response:
    pass